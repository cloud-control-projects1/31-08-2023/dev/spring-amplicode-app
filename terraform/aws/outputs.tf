output "public_url" {
  description = "Beanstalk Public URL"
  value       = join("", concat(["https://"], module.beanstalk.aws_elastic_beanstalk_environment.*.cname))
}