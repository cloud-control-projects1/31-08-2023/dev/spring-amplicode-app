module "spring-amplicode_db" {
  source = "./db"

  name                                  = var.spring-amplicode_db_name
  engine                                = var.spring-amplicode_db_engine
  engine_version                        = var.spring-amplicode_db_engine_version
  instance_class                        = var.spring-amplicode_db_instance_class
  storage                               = var.spring-amplicode_db_storage
  user                                  = var.spring-amplicode_db_user
  password                              = var.spring-amplicode_db_password
  random_password                       = var.spring-amplicode_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.spring-amplicode_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
