package com.company.springamplicode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SpringAmplicodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringAmplicodeApplication.class, args);
    }
}
